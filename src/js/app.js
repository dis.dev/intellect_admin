"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import tabs from './components/tabs.js'; // Tabs
import quantity from './forms/quantity.js' // input number
import HystModal from 'hystmodal';
import Spoilers from "./components/spoilers.js";
import Dropdown from "./components/dropdown.js";
import  { hide, show, toggle } from 'slidetoggle';

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')

// Вкладки (tabs)
tabs();

// Сворачиваемые блоки
collapse();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// input Number
quantity()

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Dropdown
Dropdown();

function searchField() {
    const searchItems = document.querySelectorAll('[data-search]');
    if (searchItems.length > 0) {
        const searchFields = document.querySelectorAll('[data-search-input]');
        searchFields.forEach(el => {
            el.addEventListener('keyup', (event) => {

                const searchBlock = event.currentTarget.closest('[data-search]');
                let searchValue = event.currentTarget.value;
                if (searchValue.length > 2) {
                    searchBlock.classList.add('open');
                    console.log(searchValue);
                }
                else {
                    searchBlock.classList.remove('open');
                }

            })
        });

        document.addEventListener('click', function (event){
            if (!event.target.closest('[data-search]')) {
                searchItems.forEach(elem => {
                    elem.classList.remove('open');
                });
            }
        });
    }
}

// Custom Scroll
// Добавляем к блоку атрибут data-simplebar
// Также можно инициализировать следующим кодом, применяя настройки




// Nav
document.addEventListener('click', (event) => {
    if (event.target.closest('[data-nav-arrow]')) {
        const navChild = event.target.closest('[data-nav-item]').querySelector('[data-nav-child]')
        toggle(
            navChild,
            {
                miliseconds: 200,
            }
        )
        event.target.closest('[data-nav-item]').classList.toggle('open')
    }
    if (event.target.closest('[data-nav-toggle]')) {
        document.querySelector('[data-root]').classList.toggle('nav-toggle')
    }
})

function fileField() {
    const fileInputs = document.querySelectorAll('[data-file]')
    if (fileInputs.length > 0) {
        fileInputs.forEach(el => {
            let fileInput = el.querySelector('[data-file-input]')
            let filePlaceholder = el.querySelector('[data-file-placeholder]')
            let fileName = ''

            fileInput.addEventListener('change', (event) => {
                fileName = fileInput.value.replace(/^.*[\\\/]/, '')
                if (fileName) {
                    filePlaceholder.innerHTML = fileName
                    el.classList.add('file-field--uploaded')
                }
                else {
                    filePlaceholder.innerHTML = filePlaceholder.dataset.filePlaceholder
                    el.classList.remove('file-field--uploaded')
                }
            })
        })
    }
}

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-filter-toggle]')) {
        document.querySelector('body').classList.toggle('filter-open')
    }
})


// Modal HystModal
const myModal = new HystModal({
    linkAttributeName: 'data-hystmodal',
});

window.addEventListener("load", function (e) {

    searchField()
    fileField()
});
